//
//  HomeViewController.swift
//  quizproject2
//
//  Created by admin on 27/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, SecondViewControllerDelegate {
    func onSave() {
        myLabel.text = "save success"
    }
    
    func onCancel() {
        myLabel.text = "cancel success"
    }
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var myLabel: UILabel!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
//    @IBAction func nextBtn(_ sender: UIButton) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let secondVC:SecondViewController = storyBoard.instantiateViewController(identifier: "SecondViewController")
//        secondVC.titleName = "from home";
//        secondVC.delegate = self;
//        present(secondVC, animated: true, completion: nil)
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ToRedSegue"){
            let secondVC:SecondViewController = segue.destination as! SecondViewController;
            secondVC.titleName = "From Home";
            secondVC.delegate = self;
        }
    }
    
    @IBAction func backFromRed(segue:UIStoryboardSegue){
        
    }
    var flag = true;
    
    @IBAction func toYellowOrBlue(_ sender: UIButton) {
        if(flag){
         performSegue(withIdentifier: "ToYellowSegue", sender: self)
            flag = false;
        }else{
              performSegue(withIdentifier: "ToBlueSegue", sender: self)
            flag = true;
        }
    }
}
