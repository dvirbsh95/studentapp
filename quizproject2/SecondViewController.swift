//
//  SecondViewController.swift
//  quizproject2
//
//  Created by admin on 27/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit

protocol SecondViewControllerDelegate {
    func onSave();
    func onCancel();
}

class SecondViewController: UIViewController {
    var titleName:String?
    @IBOutlet weak var myTitle: UILabel!
    var delegate: SecondViewControllerDelegate?;
    override func viewDidLoad() {
        super.viewDidLoad()
        
         if(titleName != nil){
             self.myTitle.text = titleName;
         }
        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "SaveSegue"){
            delegate?.onSave();
        }
        else if(segue.identifier == "CancleSegue"){
               delegate?.onCancel();
            
        }
    }
    
//    @IBAction func saveBtn(_ sender: UIButton) {
//            delegate?.onSave();
//        dismiss(animated: true, completion: nil)
//    }
//    
//    @IBAction func cancelBtn(_ sender: Any) {
//                 delegate?.onCancel();
//                    dismiss(animated: true, completion: nil)
//    }
//    
}
